
package br.com.casa.ex7.Test;

import br.com.casa.ex7.Calculadora;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class CalculadoraTest {
    
     @Test
    public void DeveEstarAprovado() {
        double nota  = 8 ;
        Calculadora calculadora = new Calculadora();
        String resultado  = calculadora.calcular(nota);
        assertEquals(Calculadora.APROVADO, resultado);
          
    }
    
    @Test
    public void deveSerEleitor() {
        double nota  = 5.5 ;
        Calculadora calculadora = new Calculadora();
        String resultado  = calculadora.calcular(nota);
        assertEquals(Calculadora.RECUPERACAO, resultado);
          
    }
    
    @Test
    public void deveSerEleitorFacultativoJovem() {
        double nota  = 3.5 ;
        Calculadora calculadora = new Calculadora();
        String resultado  = calculadora.calcular(nota);
        assertEquals(Calculadora.REPROVADO, resultado);
          
    }
}
